<?php
add_action('admin_menu', 'add_hsk_settings', "normal");

function add_hsk_settings() {
    add_menu_page("HSK Settings", "HSK Settings", "manage_options", "hsk-settings", "print_hsk_settings", 'dashicons-calendar', 50);
}

function print_hsk_settings() {
    if (count($_POST) > 0) {
      update_option("hsk_custom_css", stripslashes($_POST["custom_css"]), true);
      update_option("hsk_custom_js", stripslashes($_POST["custom_js"]), true);
    }
    $frontend = new HSK_Frontend([]);
    //Load styles
    $frontend->load_styles();
    //Getting black list options
    $controller = new HSK_Controller();
    $blacklist = $controller->get_blacklist();
    //Getting custom CSS
    $customCSS = get_option("hsk_custom_css", '');
    $customJS = get_option("hsk_custom_js", '');
    ?>
    <script>
      function remove_org_from_blacklist(e, organizerID) {
          e = e || window.event;
          e.preventDefault();
          var removeFromBlacklistURL = "<?php echo admin_url('admin-ajax.php'); ?>" + "?action=remove_from_black_list&organizerID=" + organizerID;
          var oReq = new XMLHttpRequest();
          oReq.onreadystatechange = function()
          {
              if (oReq.readyState == 4 && oReq.status == 200)
              {
                  console.log(oReq.responseText);
                  location.reload();
              }
          }; 
          oReq.open("GET", removeFromBlacklistURL, true);
          oReq.send();
      }

      function remove_event_from_blacklist(e, eventID) {
          e = e || window.event;
          e.preventDefault();
          var removeFromBlacklistURL = "<?php echo admin_url('admin-ajax.php'); ?>" + "?action=remove_from_black_list&eventID=" + eventID;
          var oReq = new XMLHttpRequest();
          oReq.onreadystatechange = function()
          {
              if (oReq.readyState == 4 && oReq.status == 200)
              {
                  console.log(oReq.responseText);
                  location.reload();
              }
          }; 
          oReq.open("GET", removeFromBlacklistURL, true);
          oReq.send();
      }
    </script>
  <h1>HSK settings</h1>
  <br>
  <h2>Shortcode generator</h2>
  <p>Copy and paste a shortcode for the events that you want to show in your page. You will find instructions on how to build a shortcode in the "Feed" page of your HvaSkjerKalender website. For example: <a target="_blank" href="https://trdevents.no/feed">https://trdevents.no/feed</a></p>

  <br>
  <h2>Black listed events</h2>
  <p>Events or organizers in the black list will be excluded from any feed in your site.</p>
  <?php if (!empty($blacklist["organizers"]) || !empty($blacklist["events"])) { ?>
    <?php if (!empty($blacklist["events"])) { ?>
    <h4>Single events</h4>
    <table cellspacing="0" style="background: white; width: 500px;">
      <tbody>
      <?php foreach($blacklist["events"] as $event) { ?>
        <tr style="height: 40px;">
          <td style="padding-left: 10px;"><?php echo $event["name"]; ?></td>
          <td style="text-align: right; padding-right: 10px;"><button onclick="remove_event_from_blacklist(event, '<?php echo $event["id"];?>')">Remove</button></td>
        </tr>
      <?php } } ?>
      </tbody>
    </table>
    
    <?php if (!empty($blacklist["organizers"])) { ?>
    <h4>Organizers</h4>
    <table cellspacing="0" style="background: white; width: 500px;">
      <tbody>
    <?php foreach($blacklist["organizers"] as $organizer) { ?>
      <tr style="height: 40px;">
          <td style="padding-left: 10px;"><?php echo $organizer["name"]; ?></td>
          <td style="text-align: right; padding-right: 10px;"><button onclick="remove_org_from_blacklist(event, '<?php echo $organizer["id"];?>')">Remove</button></td>
        </tr>
    <?php } } ?>
      </tbody>
    </table>
  <?php } else { ?>
    <p><strong>There are no events in your black list</strong></p>
  <?php } ?>

  <br>
  <form method="POST">

    <h2>Custom JS</h2>
    <p>Use the text area below to include any custom JavaScript:</p>
    <textarea name="custom_js" rows="10" style="min-width: 400px"><?php echo $customJS;?></textarea>
    <br><br><br>
    <h2>Custom CSS</h2>
    <p>Use the text area below to include any custom style:</p>
    <textarea name="custom_css" rows="10" style="min-width: 400px"><?php echo $customCSS;?></textarea>

    <div>
      <button type="submit">Save</button>
    </div>
  </form>

  <br>

  <?php
}