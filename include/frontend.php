<?php

add_action( 'wp_head', function($arguments) { 
    $currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $eventSlug = $_GET["event"];
    $source = $_GET["source"];
    if (!empty($eventSlug) && !empty($source)) {
        //Fetch the event details
        $event = (new HSK_Controller())->get_event_details($source, $eventSlug);
        //Removing the OG metas that Yoast plugin adds automatically
        add_filter( 'wpseo_opengraph_title', '__return_false');
        add_filter( 'wpseo_opengraph_desc', '__return_false');
        add_filter( 'wpseo_opengraph_url', '__return_false');
        add_filter( 'wpseo_opengraph_image', '__return_false');
        echo '<meta property="og:title" content="' . $event->title_nb . '" />' . "\n";
        echo '<meta property="og:description" content="' . strip_tags($event->desc_nb) . '" />' . "\n";
        echo '<meta property="og:image" content="' . $event->images[0]->urlLarge . '" />' . "\n";
        echo '<meta property="og:url" content="' . $currentUrl . '" />' . "\n";
    }
}, 0, 1);

class HSK_Frontend {
    
    
    const PAGE_SIZE = 6;
    public $primaryColor = '233c64';
    public $showEventsTitle = true;
    public $cache_expiration = 30;
    public $feeds = [];

    public function __construct(array $atts)
    {
        //Color set from the shortcode
        if (!empty($atts["primary_color"])) {
            $this->primaryColor = $atts["primary_color"];
        }
        //Cache expiration in minutes
        if (!empty($atts["cache_expiration"])) {
            $this->cache_expiration = $atts["cache_expiration"];
        }
        //Show the events title
        if (!empty($atts["show_events_title"])) {
            $this->showEventsTitle = ($atts["show_events_title"] === "true") ? true : false;
        }
        //Parsing the feeds options
        if (!empty($atts["feeds"])) {
            foreach(explode("||", $atts["feeds"]) as $feed) {
                //Build the feed from the shorcode attributes
                $this->feeds[] = new HSK_Feed($feed);
            }
        }
    }


    function load_styles() {
        $version = "1.0.4";
        // SimpleGrid
        wp_enqueue_style('hsk_simplegrid', HSK_URL . 'content/style/simplegrid.css', [], "v" . $version);
        // Custom CSS default style
        wp_enqueue_style('hsk_default', HSK_URL . 'content/style/default.css', [], "v" . $version);
        // Slick CSS default style
        wp_enqueue_style('hsk_slick', HSK_URL . 'content/style/slick.css', [], "v" . $version);
        // Slick CSS theme style
        wp_enqueue_style('hsk_slick_theme', HSK_URL . 'content/style/slick-theme.css', [], "v" . $version);
        // Datepicker
        wp_enqueue_style('hsk_datepicker_css', 'https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/css/datepicker.min.css', []);
    }


    function load_scripts() {
        $version = "1.0.5";
        // Datepicker library
        wp_enqueue_script( 'hsk_datepicker_js', "https://cdn.jsdelivr.net/npm/vanillajs-datepicker@1.1.4/dist/js/datepicker-full.min.js", [], "v" . $version );
        // Filter functionality
        wp_enqueue_script( 'hsk_filter_js', HSK_URL . 'content/scripts/filter.js', ['hsk_datepicker_js'], "v" . $version );
        // Slick JS
        wp_enqueue_script( 'hsk_slick_js', HSK_URL . 'content/scripts/slick.min.js', [], "v" . $version );
        // Slick JS
        wp_enqueue_script( 'hsk_images_carousel', HSK_URL . 'content/scripts/images-carousel.js', ['hsk_slick_js'], "v" . $version );
    }
    
    //Removes the country from the address string
    private function removeCountry($address) {
        return str_replace(", Norway", "", str_replace(", Norge", "", $address));
    }

    private function encodeURIComponent($str) {
        $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
        return strtr(rawurlencode($str), $revert);
    }


    private function getMunicipalities($events) {
        $municipalities = [];
        foreach($events as $event) {
            if ($event->mode === 'offline' 
            && !empty($event->venue->address)) {
                $municipality = array();
                preg_match('/\d{4}(?:,)*\s*([a-zA-ZåÅæÆøØ]+)(?:,)*/', $event->venue->address, $municipality);
                if (count($municipality) > 1 && !empty($municipality[1])) {
                    $municipalities[$municipality[1]] = 1;              
                }
            }
        }
        //TODO Fix the address formatting in the HSK. Remove Norway or Norge as a municipality meanwhile
        unset($municipalities["Norway"]);
        unset($municipalities["Norge"]);
        $municipalities = array_keys($municipalities);
        usort($municipalities, function($a, $b) {return strcmp($a, $b);});
        return $municipalities;
    }

    function print_json_ld($events) {
        if (!empty($events)) {
            
        }
    }

    //Prints a list of events
    function print_events() {
        $controller = new HSK_Controller();
        //Fetch the events
        $events = $controller->get_events($this->feeds);
        //TODO JSON+LD for metadata
        $this->print_json_ld($events);
        //JS
        $this->load_scripts();
        $this->js_load_more_events($events);
        $this->js_maybe_revalidate_cache();
        $this->js_add_to_blacklist();
        //Load custom CSS and JS
        $customCSS = get_option("hsk_custom_css", '');
        $customJS = get_option("hsk_custom_js", '');
    ?>
        <style>
            <?php echo $customCSS;?>
        </style>
        <div class="hsk-events-container">
            <?php if ($this->showEventsTitle) { ?> 
                <h1 class="en ">Events</h1>
                <h1 class="no">Arrangementer</h1>
            <?php } ?>
            <div class="grid">
            <?php 
                if (!empty($events)) {
                    echo $this->print_filter_bar($events);
                    ?>
                    <div class="grid" id="hsk-events-count-container">
                        <div class="col-6-12"><span id="hsk-events-count"><?php echo count($events);?></span> <div style="display: inline-block"><span class="en">found</span><span class="no">treff</span></div></div>
                        <div class="col-6-12" id="hsk-events-top-placeholder"></div>
                    </div>
                    <div id="hsk-events">
                    <?php
                    echo $this->get_html_events($events);
                    echo '</div>';
                } else {
                ?>
                <div class="en">No events</div>
                <div class="no">Ingen arrangement</div>
            <?php } ?>
            </div>
            <div class="grid"><div class="col-1-1 hsk-centered-button"><button id="load-more-events-btn" style="display: <?php echo count($events) < $this::PAGE_SIZE ? 'none' : 'initial';?>" onclick="loadMoreEvents()"><span class="no">Vis flere</span><span class="en">Show more</span></button></div></div>
        </div>
        <script>
            (function() {
                <?php echo $customJS;?>
            })();
        </script>
    <?php
    }

    //Prints the JS filter bar
    function print_filter_bar($events) {
        ?>
        <div id="hsk-filter-bar" class="grid">
            <div class="col-2-12">
                <label class="en" for="datefilter">From date:</label>
                <label class="no" for="datefilter">Fra dato:</label>
                <input id="datefilter" data-date="" type="text" name="datefilter">
            </div> 
            <div class="col-2-12">
                <label class="en" for="municipality">Municipality:</label>
                <label class="no" for="municipality">Kommune:</label>
                <select id="municipalities_filter"  name="municipality"> 
                    <option value="all">All</option>
                    <?php foreach($this->getMunicipalities($events) as $municipality) { ?>
                        <option value="<?php echo $municipality;?>"><?php echo $municipality;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-8-12">
                <label class="en" for="search">Search:</label>
                <label class="no" for="search">Søk:</label>
                <input id="free_search_filter" type="text" name="search">
            </div>
        </div>
        <?php
    }

    //Prints an event details
    function print_event_details($atts) {
        $controller = new HSK_Controller();
        $eventSlug = $_GET["event"];
        $source = $_GET["source"];
        $back = $_GET["back"];
        //Fetch the event details
        $event = $controller->get_event_details($source, $eventSlug);
        //JS
        $this->load_scripts();
        //TODO JSON+LD for metadata
        $this->print_json_ld([$event]);
    ?>
        <div class="hsk-events-container">
            <div id="event-details" class="grid">
            <?php 
                if (!empty($event)) {
                    echo $this->get_html_event_details($source, $event, $back);
                } else {
                ?>
                <div>No events to be shown</div>
            <?php } ?>
            </div>
        </div>
    <?php
    }

    function get_html_event_details($source, $event, $back) {
        $controller = new HSK_Controller();
        $thisUrl = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $priceLabel = $event->regularPrice == 0 ? 'Gratis' : 'CC: '.$event->regularPrice.',-';
        $priceLabel = $event->reducedPrice != 0 ? $priceLabel.' / '.$event->reducedPrice.',-' : $priceLabel;
        //Date and repetitions
        date_default_timezone_set('Europe/Oslo');
        $timestamp = strtotime($event->startDate);

        //Date in English
        $formatEN = ($event->type === 'super-event' || $event->startTime === null) ? str_replace('%O', date('S', $timestamp), "%A, %e%O %B, %Y") : str_replace('%O', date('S', $timestamp), "%A, %e%O %B, %Y @ %l:%M %p"); 
        $dateLabelEN = strftime($formatEN, $timestamp);

        //Date in Norwegian
        $res = setlocale(LC_ALL, 'nb_NO');
        $formatNO = ($event->type === 'super-event' || $event->startTime === null) ? "%A, %e. %B, %Y" : "%A, %e. %B, %Y kl %H:%M"; 
        $dateLabelNO = strftime($formatNO, $timestamp);

        $repetitions = '';
        if (count($event->repetitions) > 0) {
            $repetitions = " (+" . count($event->repetitions) . " <span class='en'>dates</span><span class='no'>datoer</span>)";
        }
        $program = [];
        if ($event->type === 'super-event') {
            $program = $controller->getProgram($source, $event->id);
        }

        ob_start();
    ?>
        <script>
            function toggleAllRepetitions() {
                var containerRep = document.getElementById("hsk-all-repetitions");
                var showButtonText = document.getElementById("hsk-show-rep-text");
                var hideButtonText = document.getElementById("hsk-hide-rep-text");
                if (containerRep.style.display === "none") {
                    containerRep.style.display = "block";
                    showButtonText.style.display = "none";
                    hideButtonText.style.display = "block";
                } else {
                    containerRep.style.display = "none";
                    showButtonText.style.display = "block";
                    hideButtonText.style.display = "none";
                }
            }
        </script>
        <?php if (!empty($back)) { ?>
            <div class="hsk-all-events">
                <a href="<?php echo $back;?>"><div class="no">< Arrangementer</div><div class="en">< Arrangementer</div></a>
            </div>
        <?php } ?>
        <div class="col-1-1">
            <div id="<?php echo $event->id;?>" class="hsk-event-details">
                <div class="hsk-event-details-image-container">
                    <?php foreach($event->images as $image) { ?>
                        <div class="hsk-event-details-image">
                            <img src="<?php echo $image->urlLarge; ?>" alt="<?php echo $image->alt; ?>" />
                            <div class="hsk-image-footer">
                                <div class="hsk-image-caption"><?php echo $image->caption;?></div>
                                <div class="hsk-image-credits"><span class="en">Credits: <?php echo $image->credits;?></span><span class="no">Foto: <?php echo $image->credits;?></span></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php if ($event->eventSoldOut) { ?>
                    <div class="hsk-event-notice en">Sold out</div>
                    <div class="hsk-event-notice no">Utsolgt</div>
                <?php } ?>
                <?php if ($event->eventCancelled) { ?>
                    <div class="hsk-event-notice en">Cancelled</div>
                    <div class="hsk-event-notice no">Avlyst</div>
                <?php } ?>
                <?php if ($event->isFieldEvent) { ?>
                    <div class="hsk-event-notice en">Field event</div>
                    <div class="hsk-event-notice no">Bransje arrangement</div>
                <?php } ?>
                <div class="hsk-event-details-upper" >
                    <div class="hsk-event-details-date"><div style="display: inline-block;"><span class="en"><?php echo $dateLabelEN . $repetitions;?></span><span class="no"><?php echo $dateLabelNO . $repetitions;?></span></div>
                       
                    </div>
                    <div id="hsk-all-repetitions" class="grid">
                        <?php foreach($event->repetitions as $repetition) { 
                            $timestamp = strtotime($repetition->startDate);
                            //Date in English
                            $formatEN = str_replace('%O', date('S', $timestamp), "%a %e%O %b, %Y @ %l:%M %p"); 
                            $repetitionDateLabelEN = strftime($formatEN, $timestamp);
                            //Date in Norwegian
                            $res = setlocale(LC_ALL, 'nb_NO');
                            $formatNO = "%a %e. %b, %Y kl %H:%M"; 
                            $repetitionDateLabelNO = strftime($formatNO, $timestamp);
                            ?>
                            <div class="col-4-12">
                                <div class="hsk-event-details-date-repetition" style="border-color: #<?php echo $this->primaryColor?>42;">
                                    <div class="hsk-event-details-date-repetition-date-time no" style="color: #<?php echo $this->primaryColor?>;"><?php echo $repetitionDateLabelNO;?></div>
                                    <div class="hsk-event-details-date-repetition-date-time en" style="color: #<?php echo $this->primaryColor?>;"><?php echo $repetitionDateLabelEN;?></div>
                                    <div class="hsk-event-details-date-repetition-venue en"><?php echo $repetition->venue->name . ($repetition->eventSoldOut ? ' <span class="hsk-event-repetition-notice">(Sold out)</span>' : ''). ($repetition->eventCancelled ? ' <span class="hsk-event-repetition-notice">(Cancelled)' : ''); ?></div>
                                    <div class="hsk-event-details-date-repetition-venue no"><?php echo $repetition->venue->name . ($repetition->eventSoldOut ? ' <span class="hsk-event-repetition-notice">(Utsolgt)</span>' : ''). ($repetition->eventCancelled ? ' <span class="hsk-event-repetition-notice">(Avlyst)</span>' : ''); ?></div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <h1 class="hsk-event-details-title no" style="color: #<?php echo $this->primaryColor?>;"><?php echo $event->title_nb;?></h1>
                    <h1 class="hsk-event-details-title en" style="color: #<?php echo $this->primaryColor?>;"><?php echo (empty($event->title_en) ? $event->title_nb : $event->title_en);?></h1>
                </div>
                <div class="hsk-event-details-content grid">
                    <div class="hsk-event-details-main col-9-12">
                        <div class="hsk-event-details-subtitle en">Details</div>
                        <div class="hsk-event-details-subtitle no">Detaljer</div>
                        <?php if (!empty($event->ticketsURL)) { ?>
                            <div class="en"><a style="background: #<?php echo $this->primaryColor?>;" target="_blank" href="<?php echo $event->ticketsURL;?>" class="hsk-event-details-cta">Buy tickets</a></div>
                            <div class="no"><a style="background: #<?php echo $this->primaryColor?>;" target="_blank" href="<?php echo $event->ticketsURL;?>" class="hsk-event-details-cta">Kjøp billetter</a></div>                                             
                        <?php } ?>
                        <div class="hsk-event-details-description no"><?php echo $event->desc_nb;?></div>
                        <div class="hsk-event-details-description en"><?php echo (empty($event->desc_en) ? $event->desc_nb : $event->desc_en);?></div>
                        <?php if (!empty($event->ticketsURL)) { ?>
                            <div class="en"><a style="background: #<?php echo $this->primaryColor?>;" target="_blank" href="<?php echo $event->ticketsURL;?>" class="hsk-event-details-cta">Buy tickets</a></div>
                            <div class="no"><a style="background: #<?php echo $this->primaryColor?>;" target="_blank" href="<?php echo $event->ticketsURL;?>" class="hsk-event-details-cta">Kjøp billetter</a></div>                                             
                        <?php } ?>
                        <?php if (!empty($event->embeddedVideoURL)) { ?>
                        <div class="hsk-event-embedded-video-container">
                            <iframe style="height: 380px; width: 100%;" src="<?php echo $controller->getVideoSource($event->embeddedVideoURL);?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>              
                        </div>
                        <?php } ?>
                        <div class="hsk-event-details-tags"><?php if (!empty($event->tags)) { foreach(explode(",", $event->tags) as $tag) { echo '<span style="background: #'.$this->primaryColor.'33; color: #'.$this->primaryColor.';" class="hsk-event-details-tag">#'.str_replace("#", "", $tag).'</span>'; } } ?></div>
                        <?php if ($event->type === 'super-event' && !empty($program)) { ?>
                            <div id="program" class="hsk-event-program">
                                <h2 style="color: #<?php echo $this->primaryColor?>;">Program (<?php echo count($program);?>)</h2>
                                
                                <?php foreach($program as $eventProgram) { 
                                    $timestamp = strtotime($eventProgram->startDate);
                                    //Date in English
                                    $formatEN = str_replace('%O', date('S', $timestamp), "%a %e%O %b, %Y @ %l:%M %p"); 
                                    $evProgramDateLabelEN = strftime($formatEN, $timestamp);
                                    //Date in Norwegian
                                    $res = setlocale(LC_ALL, 'nb_NO');
                                    $formatNO = "%a %e. %b, %Y kl %H:%M"; 
                                    $evProgramDateLabelNO = strftime($formatNO, $timestamp);
                                    ?>
                                    <div class="grid hsk-program-event">
                                        <div class="col-3-12 hsk-program-event-dates">
                                            <span class="en"><?php echo $evProgramDateLabelEN;?></span>
                                            <span class="no"><?php echo $evProgramDateLabelNO;?></span>
                                        </div>
                                        <div class="col-9-12 hsk-program-event-title">
                                            <div class="no"><a href="<?php echo $controller->getEventDetailsURL($eventProgram);?>"><?php echo $eventProgram->title_nb;?></a></div>
                                            <div class="en"><a href="<?php echo $controller->getEventDetailsURL($eventProgram);?>"><?php echo (empty($eventProgram->title_en) ? $eventProgram->title_nb : $eventProgram->title_en);?></a></div>
                                            <div class="hsk-program-event-organizer no">Arr.: <?php echo $eventProgram->organizers[0]->name;?></div>
                                            <div class="hsk-program-event-organizer en">Org.: <?php echo $eventProgram->organizers[0]->name;?></div>
                                            <?php if ($eventProgram->mode === 'online') { ?>
                                                <div>Online</div>
                                            <?php } else { ?>
                                                <div class="hsk-program-event-venue no">Sted: <?php echo $eventProgram->venue->name;?></div>
                                                <div class="hsk-program-event-venue en">Venue: <?php echo $eventProgram->venue->name;?></div>
                                            <?php } ?>
                                        </div>        
                                    </div>                                
                                <?php } ?>
                                </div>
                            
                        <?php } ?>
                    </div>
                    <div class="hsk-event-details-links col-3-12">
                        <div class="hsk-event-details-subtitle en">More info</div>
                        <div class="hsk-event-details-subtitle no">Mer info</div>
                        <!-- Share options -->
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top"  class="hsk-more-info-item-icon-col"><div><img src="<?php echo HSK_URL . 'content/icons/calendar-black.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Share</div>
                                            <div class="hsk-more-info-item-subtitle no">Del</div>
                                            <div>
                                                <a target="_blank" href="https://www.facebook.com/dialog/share?app_id=460498854604122&display=popup&href=<?php echo $this->encodeURIComponent($thisUrl);?>" style="margin-right: 10px;">Facebook</a>
                                                <a target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo $eventTitle;?>&amp;url=<?php echo $this->encodeURIComponent($thisUrl);?>">Twitter</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>

                        <?php if ($event->type === 'has-super') { 
                            $superEvent = $controller->get_event_from_id($source, $event->super_event);    
                            if (!empty($superEvent)) {
                        ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top" class="hsk-more-info-item-icon-col"><div> <img src="<?php echo HSK_URL . 'content/icons/list.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Part of <?php echo $superEvent->title_en;?></div>
                                            <div class="hsk-more-info-item-subtitle no">Del av <?php echo $superEvent->title_nb;?></div>
                                            <div>
                                                <a href="<?php echo $controller->getEventDetailsURL($superEvent);?>" style="margin-right: 10px;" class="en">See program of <?php echo $superEvent->title_en;?></a>
                                                <a href="<?php echo $controller->getEventDetailsURL($superEvent);?>" style="margin-right: 10px;" class="no">Se programet til <?php echo $superEvent->title_nb;?></a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>
                        <?php } } ?>


                        <?php if (!empty($priceLabel)) { ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top" class="hsk-more-info-item-icon-col"><div> <img src="<?php echo HSK_URL . 'content/icons/ticket.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Tickets</div>
                                            <div class="hsk-more-info-item-subtitle no">Billetter</div>
                                            <div>
                                                <?php if ($event->priceOption == 'not-available-tickets' || $event->noTicketsInfo) { ?>
                                                    <div class="hsk-more-info-item-content-name en">Tickets not available</div>
                                                    <div class="hsk-more-info-item-content-name no">Salg ikke åpnet</div>
                                                <?php } else { ?>
                                                    <div class="hsk-more-info-item-content-name"><?php echo $priceLabel;?></div>
                                                <?php } ?>
                                                <?php if (!empty($priceLabel) && !empty($event->ticketsURL)) { ?>
                                                <a target="_blank" href="<?php echo $event->ticketsURL;?>" style="margin-right: 10px;" class="en">Buy tickets</a>
                                                <a target="_blank" href="<?php echo $event->ticketsURL;?>" style="margin-right: 10px;" class="no">Kjøp billetter</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>
                        <?php } ?>


                        <?php if (!empty($event->facebookURL)) { ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top" class="hsk-more-info-item-icon-col"><div> <img src="<?php echo HSK_URL . 'content/icons/facebook-link.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Event in Facebook</div>
                                            <div class="hsk-more-info-item-subtitle no">Arrangement på Facebook</div>
                                            <div>
                                                <a target="_blank" href="<?php echo $event->facebookURL;?>" style="margin-right: 10px;" class="en">See event</a>
                                                <a target="_blank" href="<?php echo $event->facebookURL;?>" style="margin-right: 10px;" class="no">Se arrangement</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>
                        <?php } ?>

                        <?php if (!empty($event->moreInfoURL)) { ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top" class="hsk-more-info-item-icon-col"><div> <img src="<?php echo HSK_URL . 'content/icons/more-info.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">More info</div>
                                            <div class="hsk-more-info-item-subtitle no">Mer info</div>
                                            <div>
                                                <a target="_blank" href="<?php echo $event->moreInfoURL;?>" style="margin-right: 10px;" class="en">Visit website</a>
                                                <a target="_blank" href="<?php echo $event->moreInfoURL;?>" style="margin-right: 10px;" class="no">Besøk nettside</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>
                        <?php } ?>

                        <?php if (!empty($event->videosURL)) { ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top" class="hsk-more-info-item-icon-col"><div> <img src="<?php echo HSK_URL . 'content/icons/video.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Videos</div>
                                            <div class="hsk-more-info-item-subtitle no">Videoer</div>
                                            <div>
                                                <a target="_blank" href="<?php echo $event->videosURL;?>" style="margin-right: 10px;" class="en">Watch videos</a>
                                                <a target="_blank" href="<?php echo $event->videosURL;?>" style="margin-right: 10px;" class="en">Se videoer</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>
                        <?php } ?>

                        <?php if (!empty($event->organizers)) { ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top"  class="hsk-more-info-item-icon-col"><div><img src="<?php echo HSK_URL . 'content/icons/organizer.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Organizers</div>
                                            <div class="hsk-more-info-item-subtitle no">Arrangører</div>
                                            <div>
                                                <?php foreach($event->organizers as $organizer) { ?>
                                                    <div class="hsk-organizer-info">
                                                        <div class="hsk-more-info-item-content-name"><?php echo $organizer->name;?></div>
                                                        <?php if (!empty($organizer->website)) { ?>
                                                        <div class="en"><a target="_blank" href="<?php echo $organizer->website;?>">Visit organizer website</a></div>
                                                        <div class="no"><a target="_blank" href="<?php echo $organizer->website;?>">Besøk arrangørens nettside</a></div>
                                                        <?php } ?>
                                                        <?php if (!empty($organizer->telephoneNumber)) { ?>
                                                        <div><a target="_blank" href="tel:<?php echo $organizer->telephoneNumber;?>"><?php echo $organizer->telephoneNumber;?></a></div>
                                                        <?php } ?>
                                                        <?php if (!empty($organizer->email)) { ?>
                                                        <div><a target="_blank" href="mailto:<?php echo $organizer->email;?>"><?php echo $organizer->email;?></a></div>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>
                        <?php } ?>

                        <?php if ($event->ageRestriction != 'no-restriction') { ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top" class="hsk-more-info-item-icon-col"><div> <img src="<?php echo HSK_URL . 'content/icons/info.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Age restrictions</div>
                                            <div class="hsk-more-info-item-subtitle no">Aldersbegrensninger</div>
                                            <div>
                                                <?php if ($event->ageRestriction == 'minimum-age' && !empty($event->minimumAge)) { ?>
                                                <div class="hsk-more-info-item-content-name"><?php echo $event->minimumAge;?>+</div>
                                                <?php } ?>
                                                <?php if ($event->ageRestriction == 'maximum-age' && !empty($event->maximumAge)) { ?>
                                                <div class="hsk-more-info-item-content-name"><?php echo $event->maximumAge;?>-</div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>
                        <?php } ?>

                        <?php if ($event->mode == 'online') { ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top" class="hsk-more-info-item-icon-col"><div> <img src="<?php echo HSK_URL . 'content/icons/world.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Event mode</div>
                                            <div class="hsk-more-info-item-subtitle no">Arrangementsmodus</div>
                                            <div>
                                                <div class="hsk-more-info-item-content-name">Online</div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr class="hsk-more-info-itme-separator"></hr>
                        
                            <?php if (!empty($event->streamingURL)) { ?>
                            <table class="hsk-more-info-item">
                                <tbody>
                                    <tr>
                                        <td valign="top" class="hsk-more-info-item-icon-col"><div> <img src="<?php echo HSK_URL . 'content/icons/streaming.svg'; ?>" /></div></td>
                                        <td valign="top">
                                            <div class="hsk-more-info-item-content">
                                                <div class="hsk-more-info-item-subtitle">Streaming</div>
                                                <div>
                                                <div class="en"><a target="_blank" href="<?php echo $event->streamingURL;?>">Watch live stream</a></div>
                                                <div class="no"><a target="_blank" href="<?php echo $event->streamingURL;?>">Se live stream</a></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr class="hsk-more-info-itme-separator"></hr>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if (!empty($event->venue) && $event->mode != 'online') { ?>
                        <table class="hsk-more-info-item">
                            <tbody>
                                <tr>
                                    <td valign="top"  class="hsk-more-info-item-icon-col"><div><img src="<?php echo HSK_URL . 'content/icons/map-marker-black.svg'; ?>" /></div></td>
                                    <td valign="top">
                                        <div class="hsk-more-info-item-content">
                                            <div class="hsk-more-info-item-subtitle en">Venue</div>
                                            <div class="hsk-more-info-item-subtitle no">Sted</div>
                                            <div>
                                                <div class="hsk-more-info-item-content-name"><?php echo $event->venue->name;?></div>
                                                <?php if (!empty($event->venueNote)) { ?>
                                                <div class="hsk-more-info-item-content-venue-note"><?php echo $event->venueNote;?></div>
                                                <?php } ?>
                                                <?php if (!empty($event->venue->address)) { ?>
                                                <div class="hsk-more-info-item-content-venue-address"><?php echo $this->removeCountry($event->venue->address);?></div>
                                                <?php } ?>
                                                <?php if (!empty($event->venue->location)) { ?>
                                                <a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php echo $event->venue->location->latitude;?>%2C<?php echo $event->venue->location->longitude;?>">Open in Google Maps</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
        $eventStr = ob_get_contents();
        ob_end_clean();
        return $eventStr;
    }


    function get_html_events($events) {
        $eventsStr = '';
        for ($i = 0; ($i < $this::PAGE_SIZE) && ($i < count($events)); $i++) {
            $eventsStr .= $this->get_html_event($events[$i]);
        }
        return $eventsStr;
    }

    function get_html_event($event) {
        $controller = new HSK_Controller();
        $eventTitleNb = $this->truncate($event->title_nb, 45);
        $eventTitleEn = empty($event->title_en) ? $this->truncate($event->title_nb, 45) : $this->truncate($event->title_en, 45);
        $priceLabel = $event->regularPrice == 0 ? 'Gratis' : 'CC: '.$event->regularPrice.',-';
        $priceLabel = $event->reducedPrice != 0 ? $priceLabel.' / '.$event->reducedPrice.',-' : $priceLabel;
        $venueOrOnline = 'Online';
        if (!empty($event->venue)) {
            $venueOrOnline = $event->venue->name;
        }
        $urlImage = $event->images[0] ? $event->images[0]->urlLarge : '';

        //Date and repetitions
        $repetitions = '';
        if (count($event->repetitions) > 0) {
            $repetitions = " (+" . count($event->repetitions) . " <span class='en'>dates</span><span class='no'>datoer</span>)";
        }

        //Date and repetitions
        date_default_timezone_set('Europe/Oslo');
        $timestamp = strtotime($event->startDate);
        if ($event->type === 'super-event' || $event->startTime === null) {
            $format = "%e %b"; 
        } else {
            $format = "%e %b @ %H:%M"; 
        }

        //Date in English
        $dateLabelEN = strftime($format, $timestamp). $repetitions;

        //Date in Norwegian
        $res = setlocale(LC_ALL, 'nb_NO');
        $dateLabelNO = strftime($format, $timestamp). $repetitions;

        $eventDetailsURL = $controller->getEventDetailsURL($event);
        ob_start();
    ?>
        <div class="col-1-3">
            <div id="<?php echo $event->id;?>" class="hsk-event" style="background: #<?php echo $this->primaryColor?>;">
                <div class="hsk-event-image-container">
                    <?php if ($event->eventSoldOut) { ?>
                        <div class="hsk-event-ribbon en">Sold out</div>
                        <div class="hsk-event-ribbon no">Utsolgt</div>
                    <?php } ?>
                    <?php if ($event->eventCancelled) { ?>
                        <div class="hsk-event-ribbon en">Cancelled</div>
                        <div class="hsk-event-ribbon no">Avlyst</div>
                    <?php } ?>
                    <?php if ($event->isFieldEvent) { ?>
                        <div class="hsk-event-ribbon en">Field</div>
                        <div class="hsk-event-ribbon no">Bransje</div>
                    <?php } ?>
                    <a href="<?php echo $eventDetailsURL;?>">
                        <?php if (is_user_logged_in()) { ?>
                        <div class="hsk-add-to-blacklist">
                            <button type="button" onclick="add_event_to_blacklist(event, '<?php echo $event->id;?>', '<?php echo $event->title_nb;?>')">Remove event</button>
                            <?php if (!empty($event->organizers)) {
                                foreach($event->organizers as $organizer) { 
                                    if (!empty($organizer->id) && !empty($organizer->name)) { ?>
                                <button type="button" onclick="add_org_to_blacklist(event, '<?php echo $organizer->id;?>', '<?php echo $organizer->name;?>')">Remove organizer <?php echo $organizer->name;?></button>
                            <?php } } } ?>
                        </div>
                        <?php } ?>
                        <img class="hsk-event-image" src="<?php echo $urlImage; ?>" alt="<?php echo $event->images[0]->alt; ?>" />
                    </a>
                </div>
                <div class="hsk-event-title-container">
                    <a href="<?php echo $eventDetailsURL;?>"><div class="hsk-event-title en"><?php echo $eventTitleEn?></div><div class="hsk-event-title no"><?php echo $eventTitleNb?></div></a>
                </div>
                <div class="hsk-meta">
                    <div class="hsk-meta-item">
                        <img src="<?php echo ( $venueOrOnline === 'Online' ? HSK_URL . 'content/icons/world-white.svg' : HSK_URL . 'content/icons/map-marker.svg'); ?>" /><span><?php echo $venueOrOnline; ?></span>
                    </div>
                    <div class="hsk-meta-item">
                        <img src="<?php echo HSK_URL . 'content/icons/calendar.svg'; ?>" /><div style="display: inline-block;"><span class="en"><?php echo $dateLabelEN; ?></span> <span class="no"><?php echo $dateLabelNO; ?></span></div> <?php if ($event->priceOption != 'not-available-tickets') { ?> | <span><?php echo  $priceLabel;?></span> <?php } ?>
                    </div>
                    <?php if ($event->type === 'super-event') { ?>
                    <div class="hsk-meta-item">
                        <img src="<?php echo HSK_URL . 'content/icons/list-white.svg'; ?>" /><div style="display: inline-block;"><a href="<?php echo $eventDetailsURL.'#program';?>"><span class="en">See the program</span> <span class="no">Se programmet</span></a></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php
        $eventStr = ob_get_contents();
        ob_end_clean();
        return $eventStr;
    }

    function mapCats($cats, $lan) {
        $allCats = json_decode(file_get_contents(HSK_URL . "content/data/categories.json"));
        $translatedCats = [];
        foreach ($cats as $cat) {
            $translatedCats[] = $allCats->{$cat}->{$lan};
        }
        return join(", ", $translatedCats);
    }
    
    function truncate($string, $length = 100, $append = "&hellip;") {
        $string = trim($string);
        
        if (strlen($string) > $length) {
            $string = wordwrap($string, $length);
            $string = explode("\n", $string, 2);
            $string = $string[0] . $append;
        }
        return $string;
    }

    function js_load_more_events($events) {
    ?>
        <script>
            var nextPage = 1;
            var pageSize = <?php echo $this::PAGE_SIZE; ?>;
            var eventsSize = <?php echo count($events); ?>;
            var allEvents = <?php echo json_encode($events); ?>;
            var eventsHTML = <?php echo json_encode(array_map(function ($event) { return $this->get_html_event($event, $this->primaryColor); }, $events)); ?>;

            function loadMoreEvents() {
                //Iterate and add the HTML strings
                for(var i = nextPage * pageSize; i < ((nextPage + 1) * pageSize) && (i < eventsSize); i++) {
                    document.getElementById("hsk-events").innerHTML += eventsHTML[i];
                }
                //Increase the page pointer
                nextPage++;
                if (eventsSize < (nextPage * pageSize)) {
                    document.getElementById("load-more-events-btn").style.display = "none";
                }
            }
        </script>
    <?php
    }

    function js_maybe_revalidate_cache() {
    ?>
        <script>
            var feeds = <?php echo json_encode($this->feeds); ?>;
            var cache_expiration = "<?php echo $this->cache_expiration; ?>";

            window.onload = function() {
                var maybeRevalidateCacheURL = "<?php echo admin_url('admin-ajax.php'); ?>" + "?action=maybe_revalidate_cache&feeds=" + btoa(JSON.stringify(feeds)) + "&cache_expiration=" + cache_expiration;
                var oReq = new XMLHttpRequest();
                oReq.open("GET", maybeRevalidateCacheURL, true);
                oReq.send();
            }

        </script>
    <?php
    }

    function js_add_to_blacklist() {
    ?>
        <script>

            function add_org_to_blacklist(e, organizerID, organizerName) {
                e = e || window.event;
                e.preventDefault();
                var addToBlacklistURL = "<?php echo admin_url('admin-ajax.php'); ?>" + "?action=add_to_black_list&organizerID=" + organizerID + "&organizerName=" + btoa(unescape(encodeURIComponent(organizerName)));
                var oReq = new XMLHttpRequest();
                oReq.onreadystatechange = function()
                {
                    if (oReq.readyState == 4 && oReq.status == 200)
                    {
                        console.log(oReq.responseText);
                        location.reload();
                    }
                }; 
                oReq.open("GET", addToBlacklistURL, true);
                oReq.send();
            }

            function add_event_to_blacklist(e, eventID, eventTitle) {
                e = e || window.event;
                e.preventDefault();
                var addToBlacklistURL = "<?php echo admin_url('admin-ajax.php'); ?>" + "?action=add_to_black_list&eventID=" + eventID + "&eventTitle=" + btoa(unescape(encodeURIComponent(eventTitle)));
                var oReq = new XMLHttpRequest();
                oReq.onreadystatechange = function()
                {
                    if (oReq.readyState == 4 && oReq.status == 200)
                    {
                        console.log(oReq.responseText);
                        location.reload();
                    }
                }; 
                oReq.open("GET", addToBlacklistURL, true);
                oReq.send();
            }

        </script>
    <?php
    }
}