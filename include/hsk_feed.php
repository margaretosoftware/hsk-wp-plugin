<?php

class HSK_Feed {
    public $source = null;
    public $filter = null;

    public function __construct($feed)
    {
        //Parse the filter and the source from the feed string
        $this->filter = [];
        $asArr = explode( ';', $feed );
        foreach( $asArr as $val ){
            $tmp = explode( ':', $val );
            if ($tmp[1] === "true" || $tmp[1] === "false") {
                $this->filter[ $tmp[0] ] = (bool) ($tmp[1] === "true");
            } else {
                $this->filter[ $tmp[0] ] = $tmp[1];
            }
        }

        //Set the source and remove it from the filter
        $this->source = $this->filter["source"];
        unset($this->filter["source"]);

        //Convert to arrays those filter values with arrays
        if (!empty($this->filter['categories'])) {
            $this->filter['categories'] = explode(",", $this->filter['categories']);            
        }
        if (!empty($this->filter['notCategories'])) {
            $this->filter['notCategories'] = explode(",", $this->filter['notCategories']);            
        }
        if (!empty($this->filter['activityTypes'])) {
            $this->filter['activityTypes'] = explode(",", $this->filter['activityTypes']);            
        }
    }
}