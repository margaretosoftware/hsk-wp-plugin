<?php
if (!function_exists('str_contains')) {
    function str_contains($haystack, $needle) {
        return $needle !== '' && mb_strpos($haystack, $needle) !== false;
    }
}

class HSK_Controller
{
    const CACHE_FOLDER = '/cache/';
    const EVENT_LIST = 'event_list_';
    const ALL_FIELDS = '
        activityTypes
        ageRestriction
        author_id
        categories
        desc_en
        desc_nb
        duration
        editableBy
        embeddedVideoURL
        endDate
        eventCancelled
        eventSoldOut
        event_slug
        eventLink
        facebookURL
        id
        images {
            alt
            caption
            credits
            urlLarge
            urlSmall
            urlOriginal
            originalSize
            largeSize
            smallSize
        }
        isFeaturedEvent
        isFieldEvent
        maximumAge
        mazeMapPoi
        minimumAge
        mode
        moreInfoURL
        noTicketsInfo
        organizers {
            id
            email
            name
            slug
            telephoneNumber
            website
        }
        priceOption
        publishingDate
        reducedPrice
        regularPrice
        repetitions {
            startDate
            endDate
            startTime
            duration
            eventCancelled
            eventSoldOut
            mode
            ticketsURL
            streamingURL
            venue {
            address
            id
            location {
                latitude
                longitude
            }
            mapImageURL
            name
            slug
            }
        }
        startDate
        startTime
        streamingURL
        summary_en
        summary_nb
        super_event
        tags
        ticketsURL
        title_en
        title_nb
        type
        updated_at
        venue {
            address
            id
            location {
            latitude
            longitude
            }
            mapImageURL
            name
            slug
        }
        venueNote
        videosURL
    ';
    
    public function get_event_details_from_source($domain, $event_slug) {
        $query = '
        query {
            eventBySlug(eventSlug: "'.$event_slug.'") {
                '.$this::ALL_FIELDS.'
            }
        }
        ';
        
        $json = json_encode(['query' => $query]);
        
        $chObj = curl_init();
        curl_setopt($chObj, CURLOPT_URL, 'https://'.$domain.'/graphQL');
        curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chObj, CURLOPT_HEADER, true);
        curl_setopt($chObj, CURLOPT_VERBOSE, true);
        curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
        curl_setopt($chObj, CURLOPT_HTTPHEADER,
        array(
        'User-Agent: PHP Script',
        'Content-Type: application/json;charset=utf-8'
        )
        );
        
        $response = curl_exec($chObj);
        $header_size = curl_getinfo($chObj, CURLINFO_HEADER_SIZE);
        $body = substr( $response, $header_size );
        return json_decode($body)->data->eventBySlug;
    }

    public function get_events_from_source($domain, $filter) {
        if (!empty($filter)) {
            //GraphQL does want quotes on the JSON keys
            $filterNoQuotes = 'filter: '. preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:', json_encode($filter)).', ';
        } else {
            $filterNoQuotes = '';
        }
        
        $query = '
        query {
            events('.$filterNoQuotes.'page: 0, pageSize: 1000) {
                data {
                    '.$this::ALL_FIELDS.'
                }
            }
        }
        ';

        $json = json_encode(['query' => $query]);
        
        $chObj = curl_init();
        curl_setopt($chObj, CURLOPT_URL, 'https://'.$domain.'/graphQL');
        curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chObj, CURLOPT_HEADER, true);
        curl_setopt($chObj, CURLOPT_VERBOSE, true);
        curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
        curl_setopt($chObj, CURLOPT_HTTPHEADER,
        array(
            'User-Agent: PHP Script',
            'Content-Type: application/json;charset=utf-8'
            )
        );

        try {
            $response = curl_exec($chObj);
            $header_size = curl_getinfo($chObj, CURLINFO_HEADER_SIZE);
            $body = json_decode(substr( $response, $header_size ));
            $blackList = $this->get_blacklist();
            if (empty($body->errors)) {
                $events = $this->filter_events_with_blacklist($body->data->events->data, $blackList);
            } else {
                error_log("Error fetching events from ".$domain . ": " .print_r($body, true));
                $events = [];
            }
            return $events;
        } catch(Exception $ex) {
            error_log($domain . print_r($ex, true));
            return [];
        }
    }

    public function event_link_to_source($event_link) {
        $url_info = parse_url($event_link);
        return $url_info['host'];
    }

    private function filter_events_with_blacklist($eventsIn, $blackListIn) {
        $events = $eventsIn;
        if (!empty($events)) {
            global $blackList;
            $blackList = $blackListIn;
            //Remove blacklisted organizers
            $events = array_filter($events, function($event) {
                global $blackList;
                $noneBlacklistedOrganizer = true;
                if (!empty($event->organizers) && !empty($blackList["organizers"])) {
                    $organizerIDs = array_map(function($org) { return $org["id"]; }, $blackList["organizers"]);
                    foreach($event->organizers as $organizer) {
                        $noneBlacklistedOrganizer &= in_array($organizer->id, $organizerIDs) === false;
                    }
                }
                return $noneBlacklistedOrganizer;
            });
            //Remove blacklisted single events
            $events = array_filter($events, function($event) {
                global $blackList;
                if (!empty($blackList["events"])) {
                    $eventIDs = array_map(function($event) { return $event["id"]; }, $blackList["events"]);
                    return !empty($event->id) && in_array($event->id, $eventIDs) === false; 
                }
                return true;
            });
        }
        return $events;
    }

    public function remove_events_lists_caches() {
        try {
            array_map('unlink', glob(WP_CONTENT_DIR. $this::CACHE_FOLDER. $this::EVENT_LIST. '*.json'));
        } catch(Exception $e) {
            error_log(print_r($e, true));
        }
    }

    private function getFeedsString($feeds) {
        return hash('ripemd160', json_encode($feeds));
    }

    private function findEventByTitle($events, $title, $startDate){
        if (count($events) > 0) {
            foreach ( $events as $event ) {
                if ( $title === $event->title_nb && $startDate === $event->startDate) {
                    return true;
                }
            }
        }
        return false;
    }

    public function revalidate_cache_feeds($feeds, $cache_expiration = 30) {
        $feedsStr = $this->getFeedsString($feeds);
        $localPathFile = WP_CONTENT_DIR. $this::CACHE_FOLDER. $this::EVENT_LIST. $feedsStr. '.json';
        //Maybe invalidate cache
        $expiryInMinutes = $cache_expiration * 60;
        if (!file_exists($localPathFile) || (filemtime($localPathFile) < (time() - $expiryInMinutes))) {
            //Get the events from the sources
            $events = [];
            foreach ($feeds as $feed) {
                $eventsInSource = $this->get_events_from_source($feed->source, $feed->filter);
                if (!empty($eventsInSource)) {
                    foreach($eventsInSource as $newEvent) {
                        if (!$this->findEventByTitle($events, $newEvent->title_nb, $newEvent->startDate)) {
                            array_push($events, $newEvent);
                        }
                    }
                }
            }
            //Order by start date
            usort($events, function($a, $b) {return strcmp($a->startDate, $b->startDate);});
            //Write to the cache file
            file_put_contents($localPathFile, json_encode($events));
        }
    }

    public function revalidate_cache_program($source, $superEventID, $cache_expiration = 30) {
        $localPathFile = WP_CONTENT_DIR. $this::CACHE_FOLDER. $this::EVENT_LIST. $superEventID. '.json';
        //Maybe invalidate cache
        $expiryInMinutes = $cache_expiration * 60;
        if (!file_exists($localPathFile) || (filemtime($localPathFile) < (time() - $expiryInMinutes))) {
            //Get the events from the sources
            $events = $this->get_events_from_source($source, array("superEvent" => $superEventID));
            //Order by start date
            usort($events, function($a, $b) {return strcmp($a->startDate, $b->startDate);});
            //Write to the cache file
            file_put_contents($localPathFile, json_encode($events));
        }
    }

    public function get_events($feeds, $cache_expiration = 30) {
        $events = [];
        try {
            $feedsStr = $this->getFeedsString($feeds);
            $localPathFile = WP_CONTENT_DIR. $this::CACHE_FOLDER. $this::EVENT_LIST. $feedsStr. '.json';
            //Does cache file exists?
            if (!file_exists($localPathFile)) {
                $this->revalidate_cache_feeds($feeds, $cache_expiration);
            }
            $events = json_decode(file_get_contents($localPathFile));
        } catch(Exception $e) {
            error_log(print_r($e, true));
        }
        return $events;
    }

    public function get_event_from_id($source, $eventID) {
       return $this->get_event_details($source, null, null, $eventID);
    }

    public function get_event_details($source, $event_slug, $cache_expiration = 5, $event_id = null) {
        $localPathFile = WP_CONTENT_DIR. $this::CACHE_FOLDER. $event_slug. '.json';
        //Maybe invalidate cache
        $expiryInMinutes = $cache_expiration * 60;
        $event = null;
        //Does cache file exists?
        if (file_exists($localPathFile) && (filemtime($localPathFile) > (time() - $expiryInMinutes))) {
            $event = json_decode(file_get_contents($localPathFile));
        } else {
            $event = $this->get_event_details_from_source($source, $event_slug);
            $event = $this->remove_old_dates($event);
            file_put_contents($localPathFile, json_encode($event));
        }
        return $event;
    }

    public function getProgram($source, $superEventID) {
        $events = [];
        try {

            $localPathFile = WP_CONTENT_DIR. $this::CACHE_FOLDER. $superEventID. '.json';
            //Does cache file exists?
            if (!file_exists($localPathFile)) {
                $this->revalidate_cache_program($source, $superEventID, $cache_expiration);
            }
            $events = json_decode(file_get_contents($localPathFile));
        } catch(Exception $e) {
            error_log(print_r($e, true));
        }
        return $events;
    }

    public function remove_old_dates($event) {
        $eventWithModifiedDates = $event;
        if (!empty($event->repetitions)) {
            $futureRepetitions = [];
            array_unshift($event->repetitions, (object) array(
                "startDate" => $eventWithModifiedDates->startDate,
                "endDate" => $eventWithModifiedDates->endDate,
                "startTime" => $eventWithModifiedDates->startTime,
                "duration" => $eventWithModifiedDates->duration,
                "eventCancelled" => $eventWithModifiedDates->eventCancelled,
                "eventSoldOut" => $eventWithModifiedDates->eventSoldOut,
                "mode" => $eventWithModifiedDates->mode,
                "ticketsURL" => $eventWithModifiedDates->ticketsURL,
                "streamingURL" => $eventWithModifiedDates->streamingURL,
                "venue" => $eventWithModifiedDates->venue
            ));
            foreach($event->repetitions as $repetition) {
                if (strtotime($repetition->startDate) > strtotime('now')) {
                    $futureRepetitions[] = $repetition;
                }
            }
            $eventWithModifiedDates->startDate = $futureRepetitions[0]->startDate;
            $eventWithModifiedDates->endDate = $futureRepetitions[0]->endDate;
            $eventWithModifiedDates->startTime = $futureRepetitions[0]->startTime;
            $eventWithModifiedDates->duration = $futureRepetitions[0]->duration;
            $eventWithModifiedDates->mode = $futureRepetitions[0]->mode;
            $eventWithModifiedDates->ticketsURL = $futureRepetitions[0]->ticketsURL;
            $eventWithModifiedDates->streamingURL = $futureRepetitions[0]->streamingURL;
            $eventWithModifiedDates->eventSoldOut = $futureRepetitions[0]->eventSoldOut;
            $eventWithModifiedDates->eventCancelled = $futureRepetitions[0]->eventCancelled;
            $eventWithModifiedDates->venue = $futureRepetitions[0]->venue;
            array_shift($futureRepetitions);
            $eventWithModifiedDates->repetitions = $futureRepetitions;
        }
        return $eventWithModifiedDates;
    }
    
    public function load_ajax() {
        add_action('wp_ajax_maybe_revalidate_cache', array( $this, 'maybe_revalidate_cache_callback' ));
        add_action('wp_ajax_nopriv_maybe_revalidate_cache', array( $this, 'maybe_revalidate_cache_callback' ));
        add_action('wp_ajax_add_to_black_list', array( $this, 'add_to_black_list_callback' ));
        add_action('wp_ajax_nopriv_add_to_black_list', array( $this, 'add_to_black_list_callback' ));
        add_action('wp_ajax_remove_from_black_list', array( $this, 'remove_from_black_list_callback' ));
        add_action('wp_ajax_nopriv_remove_from_black_list', array( $this, 'remove_from_black_list_callback' ));
    }
    
    public function maybe_revalidate_cache_callback() {
        try {
            $feeds = json_decode(base64_decode($_GET["feeds"]));
            $cache_expiration = $_GET["cache_expiration"];
            $this->revalidate_cache_feeds($feeds, $cache_expiration);
            echo "{ success: true }";
        } catch(Exception $e) {
            echo "{ success: false }";
        }
        wp_die(); // this is required to terminate immediately and return a proper response
    }
    
    public function get_blacklist() {
        $blackList = get_option("hsk_blacklist");
        if (empty($blackList)) {
            $blackList = [
                "organizers" => [],
                "events" => []
            ];
        } else {
            $blackList = json_decode($blackList, true);
        }
        return $blackList;
    }

    public function add_to_black_list_callback() {
        try {
            $organizerID = $_GET["organizerID"];
            $organizerName = base64_decode($_GET["organizerName"]);
            $eventID = $_GET["eventID"];
            $eventTitle = base64_decode($_GET["eventTitle"]);
            $blackList = $this->get_blacklist();
            if (!empty($organizerID)) {
                array_push($blackList["organizers"], array('id' => $organizerID, 'name' => $organizerName));
            }
            if (!empty($eventID)) {
                array_push($blackList["events"], array('id' => $eventID, 'name' => $eventTitle));
            }
            update_option("hsk_blacklist", json_encode($blackList), true);
            $this->remove_events_lists_caches();
            echo "{ success: true }";
        } catch(Exception $e) {
            echo "{ success: false }";
        }
        wp_die(); // this is required to terminate immediately and return a proper response
    }


    public function remove_from_black_list_callback() {
        try {
            $organizerID = $_GET["organizerID"];
            $eventID = $_GET["eventID"];
            $blackList = $this->get_blacklist();
            if (!empty($organizerID)) {
                foreach ($blackList["organizers"] as $org_key => $organizer) {
                    if ($organizer["id"] === $organizerID) {
                        unset($blackList["organizers"][$org_key]);
                    }
                }
            }
            if (!empty($eventID)) {
                foreach ($blackList["events"] as $event_key => $event) {
                    if ($event["id"] === $eventID) {
                        unset($blackList["events"][$event_key]);
                    }
                }
            }
            update_option("hsk_blacklist", json_encode($blackList), true);
            $this->remove_events_lists_caches();
            echo "{ success: true }";
        } catch(Exception $e) {
            echo "{ success: false }";
        }
        wp_die(); // this is required to terminate immediately and return a proper response
    }

    public function  getVideoSource($embeddedVideoURL) {
        if (str_contains($embeddedVideoURL, "youtu")) {
            //It's a youtube video
            $videoURL = explode("v=", $embeddedVideoURL);
            //Short version of the youtube video URL
            if (str_contains($embeddedVideoURL, "youtu.be/")) {
              $videoURL = explode(".be/", $embeddedVideoURL);;
            }
            if (count($videoURL) > 0) {
              $videoID = $videoURL[1];
            }
            if (!empty($videoID)) {
              $ampersandPosition = strpos($videoID, "&");
              if ($ampersandPosition !== false) {
                $videoID = substr($videoID, 0, $ampersandPosition);
              }
              return 'https://www.youtube.com/embed/' . $videoID;
            }
        }
        if (str_contains($embeddedVideoURL, "vimeo")) {
            //It's a vimeo video
            $videoURL = explode("vimeo.com/", $embeddedVideoURL);
            if (count($videoURL) > 0) {
                $videoID = $videoURL[1];
            }
            if ($videoID) {
                $ampersandPosition = strpos($videoID, '&');
                if ($ampersandPosition !== false) {
                $videoID = substr($videoID, 0, $ampersandPosition);
                }
                return 'https://player.vimeo.com/video/' . $videoID;
            }
        }
        return "";
    }

    public function getEventDetailsURL($event) {
        return get_permalink() . '?source='.$this->event_link_to_source($event->eventLink) . "&event=" . $event->event_slug . "&back=" . get_permalink();
    }

}