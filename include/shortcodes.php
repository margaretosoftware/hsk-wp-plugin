<?php

class HSK_Shortcodes {
    function load() {
        add_shortcode('show-hsk-events', array( $this, 'showEvents' ));
    }
    
    function showEvents($atts, $content = null) {
        $frontend = new HSK_Frontend($atts);
        //Load styles
        $frontend->load_styles();
        if (!empty($_GET["event"]) && !empty($_GET["source"])) {
            //HTML
            $frontend->print_event_details($atts);
        } else {
            //HTML
            $frontend->print_events();
        }
    }
}