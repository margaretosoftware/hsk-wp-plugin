<?php
/**
* Plugin Name: HSK Plugin
* Plugin URI: https://www.hvaskjerkalender.no
* Description: Import and show events from any HSK license
* Version: 1.0
* Author: Pablo Margareto
* Author URI: https://www.hvaskjerkalender.no
*/

if ( ! defined( 'ABSPATH' ) )
    exit;

define( "HSK_URL", plugin_dir_url( __FILE__ ) );

function hsk_init(){
    $hsk = new HSK();
    $hsk->load();
}
add_action('plugins_loaded', 'hsk_init', 0 );

class HSK {
    function load() {
        
        //Include all the classes
        foreach ( glob( plugin_dir_path( __FILE__ ) . "include/*.php" ) as $file ) {
			include_once( $file );
		}

        //Load shortcodes
        (new HSK_Shortcodes())->load();

        //Load AJAX functions
        (new HSK_Controller())->load_ajax();
    }
}
