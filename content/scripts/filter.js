//Initial values
var selectedMunicipality = 'all';
var selectedFromDate = (new Date()).getTime();
var freeSearchText = '';
var extraFiltersCallbacks = [];

//Setup the date from filter
function filterbar_setup_datefilter() {
    const datepicketInput = document.querySelector('input[name="datefilter"]');
    if (datepicketInput) {
        const datepicker = new Datepicker(datepicketInput, {
            autohide: true,
            clearBtn: true,
            minDate: new Date(),
            todayBtn: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        datepicker.setDate(new Date());
        datepicketInput.addEventListener("changeDate", function () {
            selectedFromDate = datepicker.getDate().getTime();
            execute_filter();
        });
    }
}

//Setup the municipalities filter listener
function filterbar_setup_municipalities_filter() {
    const municipalities_filter = document.getElementById('municipalities_filter');
    if (municipalities_filter) {
        municipalities_filter.addEventListener("change", function () {
            selectedMunicipality = municipalities_filter.value;
            execute_filter();
        });
    }
}

//Setup the free text filter listener
function filterbar_setup_free_search_filter() {
    const free_search_filter = document.getElementById('free_search_filter');
    if (free_search_filter) {
        free_search_filter.addEventListener("input", function () {
            freeSearchText = free_search_filter.value;
            execute_filter();
        });
    }
}

//Executes the filtering with the given parameters
function execute_filter() {
    var selectedIndexes = [];
    allEvents.map((hsk_event, index) => {
        var municipalityCond = true, dateCond = true, freeSearchCond = true, extraFilters = true;
        if (selectedMunicipality != "all" && hsk_event.venue && hsk_event.venue.address) {
            municipalityCond = hsk_event.venue.address.toLowerCase().indexOf(selectedMunicipality.toLowerCase()) != -1;
        }
        dateCond = selectedFromDate < getEventEndDate(hsk_event.endDate).getTime();

        if (freeSearchText !== '') {
            freeSearchCond = (hsk_event.categories.join().toLowerCase().indexOf(freeSearchText.toLowerCase()) != -1)
                || (hsk_event.title_nb.toLowerCase().indexOf(freeSearchText.toLowerCase()) != -1)
                || (hsk_event.title_en.toLowerCase().indexOf(freeSearchText.toLowerCase()) != -1)
                || (hsk_event.desc_nb.toLowerCase().indexOf(freeSearchText.toLowerCase()) != -1)
                || (hsk_event.desc_en.toLowerCase().indexOf(freeSearchText.toLowerCase()) != -1);
        }
        //Check if any of the extra filters does not pass the condition on this event
        if (extraFiltersCallbacks.length > 0) {
            extraFiltersCallbacks.forEach((value) => {
                if (value.filterFunction) {
                    if (!value.filterFunction(hsk_event)) {
                        extraFilters = false;
                    }
                }
            });
        }
        if (municipalityCond && dateCond && freeSearchCond && extraFilters) {
            selectedIndexes.push(index);
        }
    });
    document.getElementById("hsk-events").innerHTML = selectedIndexes.map((i) => eventsHTML[i]).join("");
    document.getElementById("hsk-events-count").innerHTML = selectedIndexes.length;
    if (eventsSize != selectedIndexes.length) {
        document.getElementById("load-more-events-btn").style.display = "none";
    } else {
        document.getElementById("load-more-events-btn").style.display = "inline-block";
    }
}

//Parse date compatible with all browsers
function getEventEndDate(strDate) {
    var a = strDate.split(/[^0-9]/);
    return new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
}

filterbar_setup_datefilter();
filterbar_setup_municipalities_filter();
filterbar_setup_free_search_filter();