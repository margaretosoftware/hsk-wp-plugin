# Hva skjer kalender Wordpress Plugin
This is a plugin that let you show events from any license of [HvaSkjerKalender](https://hvaskjerkalender.no) in your website.

# Instructions
1. Write the following shortcode on the page that shall show the list of events that you want:
´[show-hsk-events primary_color="HEX_CODE_COLOR" feeds="source:DOMAIN_SOURCE;att:value" cache_expiration="30"]´. Multiple feeds can be defined.
2. Create a page that has the events listing page as a parent and introduce the following shortcode:
´[show-hsk-event-details primary_color="233c64"]´


# Glossary
1. HEX_CODE_COLOR would be the primary color to be used on the list of the events. Example: '233c64'
2. DOMAIN_SOURCE would be the domain of a valid licens. Example: trdevents.no
3. att:value would be a pair of valid properties of a filter [more info here > GraphQl > Schema > Filter](https://trdevents.no/feed).
4. cache_expirarion would be the expiration of the cache in minutes. Once the cache is expired, the plugin will fetch the events again.

# Example of shortcodes
1. All the events from TRDevents.no within the food or concert category:
´[show-hsk-events primary_color="233c64" feeds="source:trdevents.no;categories:FOOD,CONCERT" cache_expiration="30"]´
2. All the events from HvaSkjerISteinkjer.no or TRDEvents.no that are not within the family category:
´[show-hsk-events primary_color="233c64" feeds="source:trdevents.no;notCategories:FAMILY||source:hvaskjeristeinkjer.no;notCategories:FAMILY" cache_expiration="30"]´